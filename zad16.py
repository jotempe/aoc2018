class Processor:
    def __init__(self):
        self.reg = [0,0,0,0]
        self.instrs = [self.addr, self.addi, self.mulr, self.muli,
                       self.banr, self.bani, self.borr, self.bori,
                       self.setr, self.seti, self.gtir, self.gtri,
                       self.gtrr, self.eqir, self.eqri, self.eqrr]

    def setreg(self,data):
        assert(len(data) == 4)
        for i in range(0,4):
            self.reg[i] = data[i]

    def getreg(self):
        return self.reg.copy()

    def addr(self,a,b,c):
        self.reg[c] = self.reg[a] + self.reg[b]

    def addi(self,a,b,c):
        self.reg[c] = self.reg[a] + b

    def mulr(self,a,b,c):
        self.reg[c] = self.reg[a] * self.reg[b]

    def muli(self,a,b,c):
        self.reg[c] = self.reg[a] * b

    def banr(self,a,b,c):
        self.reg[c] = self.reg[a] & self.reg[b]

    def bani(self,a,b,c):
        self.reg[c] = self.reg[a] & b

    def borr(self,a,b,c):
        self.reg[c] = self.reg[a] | self.reg[b]

    def bori(self,a,b,c):
        self.reg[c] = self.reg[a] | b

    def setr(self,a,b,c):
        self.reg[c] = self.reg[a]

    def seti(self,a,b,c):
        self.reg[c] = a

    def gtir(self,a,b,c):
        self.reg[c] = 1 if a > self.reg[b] else 0

    def gtri(self,a,b,c):
        self.reg[c] = 1 if self.reg[a] > b else 0

    def gtrr(self,a,b,c):
        self.reg[c] = 1 if self.reg[a] > self.reg[b] else 0

    def eqir(self,a,b,c):
        self.reg[c] = 1 if a == self.reg[b] else 0

    def eqri(self,a,b,c):
        self.reg[c] = 1 if self.reg[a] == b else 0

    def eqrr(self,a,b,c):
        self.reg[c] = 1 if self.reg[a] == self.reg[b] else 0


p = Processor()
filein=open("input16-1.txt")
ntest = 0
opcodes = [p.instrs.copy() for i in range(0,16)]
for l in filein:
    ntest = ntest+1
    assert(l[0:6]=="Before")
    init = eval(l[8:-1])
    op,a,b,c = (int(x) for x in filein.readline()[:-1].split(' '))
    l = filein.readline()
    assert(l[0:5]=="After")
    final = eval(l[8:-1])
    l = filein.readline()
    for f in opcodes[op]:
        p.setreg(init)
        f(a,b,c)
        res = p.getreg()
        if res[c] != final[c]:
            opcodes[op].remove(f)
for op in opcodes:
    print([x.__name__ for x in op])
print('---- sherlock working ----')
changed = True
while changed:
    changed = False
    for op in opcodes:
        if len(op) == 1:
            for op2 in opcodes:
                if len(op2)>1 and op[0] in op2:
                    op2.remove(op[0])
                    changed = True

instr = [op[0] for op in opcodes]
for i in range(0,16):
    print(i," ",instr[i].__name__)

filein.close()
filein = open("input16-2.txt")
p.setreg([0,0,0,0])
nstep = 0
for l in filein:
    op,a,b,c = (int(x) for x in l[:-1].split(' '))
    instr[op](a,b,c)
    nstep = nstep+1
print("program steps:",nstep)
print("final state",p.getreg())
