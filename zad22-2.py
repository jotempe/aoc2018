# Dijkstra algorithm ze zmienianiem wyposażenia.
# Każdy nod mapy identyfiowany jest trzeba danymi: dwie współrzędne i używany
# sprzęt. 0 - żaden, 1 - pochodnia, 2 - wspinaczka
# sąsiedztwo nodów: sąsiedni jest każdy z 4 sąsiednich we współrzędnych
# i tym samym sprzętem o ile można wejść. I sąsiedni jest ten z tymi samymi
# współrzędnymi i innym sprzętem.
def readmap(filename,maxx,maxy):
    mapa = list()
    nodemap = list
    for line in open(filename):
        if len(mapa) >= maxy : break
        row = [int(c) for c in line.split(' ')]
        if len(row) > maxx : row = [row[i] for i in range(0,maxx)]
        mapa.append(row)
    return(mapa)
        
def makenodelist(mapa,xtar,ytar,gtar):
    nodemap = list()
    nodelist = list()
    for row in mapa:
        noderow = list()
        y = len(nodemap)
        nodemap.append(noderow)
        for loc in row:
            nodeloc = list()
            x = len(noderow)
            noderow.append(nodeloc)
            for gear in range(0,3):
                if gear != mapa[y][x] :
                    n = Node(x,y,gear)
                    if y > 0:
                        nnode = nodemap[y-1][x][gear] 
                        if isinstance(nnode,Node):
                            n.addneighbor(nnode,1)
                    if x > 0:
                        nnode = noderow[x-1][gear]
                        if isinstance(nnode,Node):
                            n.addneighbor(nnode,1)
                    for nnode in nodeloc:
                        if isinstance(nnode,Node):
                            n.addneighbor(nnode,7)
                    nodeloc.append(n)
                    nodelist.append(n)
                    if (x==xtar) and (y==ytar) and (gear==gtar):
                        n.istarget = True
                else : nodeloc.append(gear)
    return(nodelist)

class Node:
    def __init__(self,x,y,gear):
        self.x = x
        self.y = y
        self.gear = gear
        self.visited = False
        self.istarget = False
        self.dist = 99999999
        self.prev = None
        self.neighbors = list()

    def __str__(self):
        return("x:{0}, y:{1}, g:{2} d:{3} v:{4}".format(self.x,self.y,self.gear,self.dist,self.visited))

    def addneighbor(self,nnode,cost):
        self.neighbors.append((nnode,cost))
        nnode.neighbors.append((self,cost))

mapa = readmap("map22.txt",199,799)
xtar = 8
ytar = 713
mapa[ytar][xtar] = 0
nl = makenodelist(mapa,xtar,ytar,1)
nl[0].dist = 0
unvisited = len(nl)
tosearch = [nl[0]]
while unvisited>0 :
    node = min(tosearch,key = lambda x: x.dist)
    if node.istarget: break
    node.visited = True
    tosearch.remove(node)
    unvisited = unvisited-1
    if (len(nl)-unvisited) % 100 == 0:
        print("nodes left: ",unvisited)
    for nnode,cost in node.neighbors:
        if (not nnode.visited) and node.dist+cost < nnode.dist:
            nnode.dist = node.dist + cost
            nnode.prev = node
            if not (nnode in tosearch) : tosearch.append(nnode)

for n in nl:
    if n.istarget:
        print("target: ",n)
        break
