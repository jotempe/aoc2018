map = list()
carts = list()

class Cart:
    def __init__(self,px,py,hd):
        #      3
        #      ^
        #    2<.>0
        #      v
        #      1
        self.x = px
        self.y = py
        if   (hd=='>'): self.head = 0
        elif (hd=='v'): self.head = 1
        elif (hd=='<'): self.head = 2
        elif (hd=='^'): self.head = 3
        else: raise ValueError
        self.turn = -1
        self.active = True

    def move(self):
        if   (self.head==0): self.x = self.x+1
        elif (self.head==1): self.y = self.y+1
        elif (self.head==2): self.x = self.x-1
        else: self.y = self.y-1

        loc = map[self.y][self.x]
        if   (loc == '-' or loc == '|'): pass
        elif (loc == '/'):
            self.head = 3-self.head
        elif (loc == '\\'):
            if (self.head>1): self.head = 5-self.head
            else: self.head = 1-self.head
        elif (loc == '+'):
            self.head = self.head+self.turn
            if (self.head>3): self.head = 0
            if (self.head<0): self.head = 3
            self.turn = self.turn+1
            if (self.turn>1): self.turn = -1
        else:
            raise ValueError

    def collide(self,other):
        return (self.x == other.x and self.y == other.y)

def cartloc(c):
    return c.y*xmax + c.x

file = open('input13.txt')
y = 0
cchars = ('^','v','>','<')
for line in file:
    ll = list(line)
    for x in range(0,len(ll)):
        if (ll[x] in cchars):
            carts.append(Cart(x,y,ll[x]))
            print ("cart at",x,y)
            ll[x] = '-'
    map.append(ll)
    y = y+1
xmax = len(map[0])
print ("map ready, size",xmax,len(map))
print ("number carts",len(carts))

tick = 0
coll = False
try:
    while True:
        tick = tick+1
        for c in sorted(carts,key=cartloc):
            if c.active:
                c.move()
                for d in carts:
                    if c != d and c.collide(d):
                        print ("collision at",c.x,c.y)
                        d.active = False
                        c.active = False
                        carts.remove(c)
                        carts.remove(d)
                        break
            else:
                print("inactive at",c.x,c.y)
        if len(carts)==1:
            print("last cart at",c.x,c.y,"tick",tick)
            break
            
except:
    print("failed at",c.x,c.y,map[c.y][c.x])
print("end")
