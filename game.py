def game(plcount,highmar):
    score = {p:0 for p in range(1,plcount+1)}
    circle = [0,1]
    cplay = 2
    cpos = 1
    for m in range(2,highmar+1):
        npos = cpos+1
        if (m % 23) == 0 :
            score[cplay] = score[cplay] + m
            cpos = cpos-7
            if cpos < 0 : cpos = cpos + len(circle)
            score[cplay] = score[cplay] + circle[cpos]
            del circle[cpos]
            if cpos==len(circle) : cpos = 0
        else:
            cpos = cpos+2
            if cpos>len(circle)+1 : cpos = cpos-len(circle)
            circle.insert(cpos,m)
        cplay = cplay+1
        if cplay>plcount : cplay = 1
    maxscore = 0
    winner = 0
    for p in range(1,plcount+1):
        if score[p] > maxscore:
            maxscore = score[p]
            winner = p
    print("winner:",winner," score:",maxscore)
