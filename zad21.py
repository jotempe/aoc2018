class Processor:
    def __init__(self):
        self.reg = [0,0,0,0,0,0]
        self.instrs = [self.addr, self.addi, self.mulr, self.muli,
                       self.banr, self.bani, self.borr, self.bori,
                       self.setr, self.seti, self.gtir, self.gtri,
                       self.gtrr, self.eqir, self.eqri, self.eqrr]
        self.ip = 0

    def setreg(self,data):
        assert(len(data) == 6)
        for i in range(0,6):
            self.reg[i] = data[i]

    def getreg(self):
        return self.reg.copy()

    def addr(self,a,b,c):
        self.reg[c] = self.reg[a] + self.reg[b]

    def addi(self,a,b,c):
        self.reg[c] = self.reg[a] + b

    def mulr(self,a,b,c):
        self.reg[c] = self.reg[a] * self.reg[b]

    def muli(self,a,b,c):
        self.reg[c] = self.reg[a] * b

    def banr(self,a,b,c):
        self.reg[c] = self.reg[a] & self.reg[b]

    def bani(self,a,b,c):
        self.reg[c] = self.reg[a] & b

    def borr(self,a,b,c):
        self.reg[c] = self.reg[a] | self.reg[b]

    def bori(self,a,b,c):
        self.reg[c] = self.reg[a] | b

    def setr(self,a,b,c):
        self.reg[c] = self.reg[a]

    def seti(self,a,b,c):
        self.reg[c] = a

    def gtir(self,a,b,c):
        self.reg[c] = 1 if a > self.reg[b] else 0

    def gtri(self,a,b,c):
        self.reg[c] = 1 if self.reg[a] > b else 0

    def gtrr(self,a,b,c):
        self.reg[c] = 1 if self.reg[a] > self.reg[b] else 0

    def eqir(self,a,b,c):
        self.reg[c] = 1 if a == self.reg[b] else 0

    def eqri(self,a,b,c):
        self.reg[c] = 1 if self.reg[a] == b else 0

    def eqrr(self,a,b,c):
        self.reg[c] = 1 if self.reg[a] == self.reg[b] else 0

    def execute(self,program,ipr):
        # Program - lista instrukcji
        # ipr - numer rejestru zawierającego IP
        steps = 0
        while self.ip<len(program):
            self.reg[ipr] = self.ip
            curr = program[self.ip]
            getattr(self, curr[0]) (curr[1],curr[2],curr[3])
            self.ip = self.reg[ipr]
            self.ip = self.ip+1
            steps = steps + 1
        return steps

    def debug(self,program,ipr):
        steps = 0
        bpoints = list()
        cmd = ''
        trace = False
        while cmd != 'q' :
            inline = input(str(steps)+str(self.getreg())+'>').split(' ')
            cmd = inline[0]
            if cmd == 'r':
                regn = int(inline[1])
                val = int(inline[2])
                self.reg[regn] = val
            elif cmd == 'b':
                bpoints.append(int(inline[1]))
                print("breakpoints",bpoints)
            elif cmd == 'd':
                addr = int(inline[1])
                if addr in bpoints: bpoints.remove(addr)
                print("breakpoints",bpoints)
            elif cmd == 'g':
                if len(inline)>1:
                    laststep = steps+int(inline[1])
                else:
                    laststep = 100000000000
                while steps<laststep and self.ip<len(program):
                    self.reg[ipr] = self.ip
                    curr = program[self.ip]
                    if trace:
                        print(self.ip,":",self.reg,curr)
                    getattr(self, curr[0]) (curr[1],curr[2],curr[3])
                    self.ip = self.reg[ipr]
                    self.ip = self.ip+1
                    steps = steps + 1
                    if self.ip in bpoints:
                        print("breakpoint at:",self.ip)
                        break
                else:
                    if self.ip>=len(program) : print('halt')
                    else: print("step limit reached")
            elif cmd=='s':
                self.reg[ipr] = self.ip
                curr = program[self.ip]
                if trace:
                    print(self.ip,":",self.reg,curr)
                getattr(self, curr[0]) (curr[1],curr[2],curr[3])
                self.ip = self.reg[ipr]
                self.ip = self.ip+1
                steps = steps+1
            elif cmd=='t':
                if inline[1]=='0' : trace = False
                else: trace = True
                print("trace",trace)
            elif cmd=='l':
                endl = len(program)
                if len(inline)>2 : endl=min(int(inline[2]),len(program))
                startl = 0
                if len(inline)>1 : startl=min(int(inline[1]),len(program)-1)
                for i in range(startl,endl):
                    print(i,":",prog[i])
            elif cmd=='q':
                print('quit')
            else:
                print("bad command")           

p = Processor()
filein=open("input21.txt")
#fileout=open("out21.txt",mode="w")
prog = list()
for line in filein:
    if (line.startswith("#ip")):
        ipr = int(line.split(' ')[1])
    else:
        s = line.split(' ')
        instr = [s[0]] + [int(i) for i in s[1:]]
        prog.append(instr)

for l in prog:
    print(l)

p.setreg([0,0,0,0,0,0])
p.debug(prog,ipr)
