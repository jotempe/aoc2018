class Group:
    def __init__(self, count, HP, dam, AT, weak, immunity, initiative, gname):
        self.groupname = gname
        self.unitcount = count
        self.HP = HP
        self.damage = dam
        self.AT = AT
        self.weakness = weak
        self.immunity = immunity
        self.initiative = initiative
        self.target = None
        self.targeted = False
        self.dead = False

    def takedamage(self, dam):
        kills = dam // self.HP
        self.unitcount = self.unitcount - kills
        if self.unitcount <= 0:
            self.unitcount = 0
            self.dead = True

    def selecttarget(self,enemies):
        effpower = self.damage*self.unitcount
        targets = list()
        for group in [e for e in enemies if not e.targeted] :
            attpoint = effpower
            if self.AT in group.weakness:
                attpoint = attpoint * 2
            if self.AT in group.immunity:
                attpoint = 0
            if attpoint>0 :
                targets.append((group, attpoint))

        if len(targets)>0:
            maxdam = -1
            maxpow = 0
            maxini = 0
            for t,ap in targets:
                if ap > maxdam:
                    self.target = t
                    maxdam = ap
                    maxpow = t.unitcount*t.damage
                    maxini = t.initiative
                elif ap == maxdam :
                    epow = t.unitcount*t.damage
                    if epow>maxpow :
                        self.target = t
                        maxpow = epow
                        maxini = t.initiative
                    elif epow == maxpow:
                        if t.initiative > maxini:
                            self.target = t
                            maxini = t.initiative
            if self.target != None:
                self.target.targeted = True

    def attack(self):
        if not self.dead and self.target != None:
            attpoint = self.unitcount*self.damage
            if self.AT in self.target.weakness : attpoint = attpoint*2
            self.target.takedamage(attpoint)
            self.target = None

class InfGroup(Group):
    def side(self):
        return "Infection"

class ImmGroup(Group):
    def side(self):
        return "Immune System"

def readinput(file):
    inflist = list()
    immlist = list()
    boost = int(input("boost?"))

    l = immlist
    for line in open(file):
        print(line)
        if line.startswith("Infe"):
            l = inflist
        else:
            data = line.split(' ')
            cnt = int(data[0])
            HP = int(data[1])
            weakness = data[2].split(',')
            immunity = data[3].split(',')
            dam = int(data[4])
            AT = data[5]
            init = int(data[6])
            if l is immlist: l.append(ImmGroup(cnt, HP, dam+boost, AT, weakness, immunity, init, "group "+str(len(l)+1)))
            else: l.append(InfGroup(cnt, HP, dam, AT, weakness, immunity, init, "group "+str(len(l)+1)))
        
    return (inflist,immlist)

indata = readinput("input24.txt")
inflist = indata[0]
immlist = indata[1]
for g in immlist+inflist:
    print ("{} {}: {} units at {}, weak {}, immune {}, init {}".format(g.side(), g.groupname, g.unitcount, 
                                                                       g.HP, g.weakness, g.immunity, g.initiative))

while len(inflist) > 0 and len(immlist) > 0:
    for g in sorted(inflist+immlist, reverse=True, key = lambda x: x.unitcount*x.damage*100 + x.initiative):
        if isinstance(g,InfGroup) : g.selecttarget(immlist)
        else : g.selecttarget(inflist)

    for g in sorted(inflist+immlist, reverse=True, key = lambda x: x.initiative):
        g.attack()

    for l in (inflist,immlist):
        for g in l.copy():
            if g.dead : l.remove(g)
            else : g.targeted = False                                                                                                         

if len(inflist) > 0:
    print("Infection wins")
    l = inflist
else:
    print("Immunity wins")
    l = immlist

print ("result ", sum([g.unitcount for g in l]))

