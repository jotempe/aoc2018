class Alt:
    def __init__(self):
        self.alts = list()
    def addalt(self,alt):
        self.alts.append(alt)
    def __repr__(self):
        return "Alt"+self.alts.__repr__()

def buildtree(root):
    c = ''
    while c != '$':
        c = infile.read(1)
        if c=='(':
            newalt = Alt()
            root.append(newalt)
            buildalt(newalt)
        elif c=='|' or c==')':
            return(c)
        elif c in ('N','S','E','W'):
            root.append(c)
    return(c)

def buildalt(alt):
    c = ''
    newnode = list()
    while c != '$':
        c = buildtree(newnode)
        alt.addalt(newnode)
        if c=='|':
            newnode = list()
        elif c == ')' or c=='$':
            return

def prunetree(tree):
    for i in range(1,len(tree)):
        if isinstance(tree[i],Alt) and len(tree[i].alts[-1]) == 0:
            for l in tree[i].alts[:-1]:
                assert isinstance(l,list)
                cut = len(l)//2
                del l[cut:]
            tree[i].alts[-1] = tree[(i+1):]
            del tree[(i+1):]
            prunetree(tree[i].alts[-1])
            break
        elif isinstance(tree[i],Alt):
            for l in tree[i].alts:
                prunetree(l)
        

def treedepth(tree):
    length = 0
    for x in tree:
        if isinstance(x,Alt):
            maxlen = 0
            for y in x.alts:
                ylen = treedepth(y)
                if ylen > maxlen : maxlen = ylen
            length = length + maxlen
        elif isinstance(x,list):
            length = length + treedepth(x)
        else:
            length = length + 1
    return length

infile = open("input20.txt")
c = infile.read(1)
print(c)
tree = list()
c = buildtree(tree)
print(c)
prunetree(tree)
print(treedepth(tree))
