adjoffs = ((0,-1),(-1,0),(1,0),(0,1))
def AdjFields(x,y):
    return [(x+a,y+b) for (a,b) in adjoffs]

def readboard(file):
    board = list()
    for line in open(file):
        board.append(list(line[:-1]))
    return board

def printboard(board):
    for line in board:
        print(''.join(c for c in line))

class Field:
    def __init__(self,x,y,dist,path):
        self.x = x
        self.y = y
        self.dist = dist
        self.path = path
        self.walked = False
    def Walk(self,enemy,avfields,aven,avenp):
        #print("Walk field:",self)
        nei = AdjFields(self.x,self.y)
        for (x,y) in nei:
            if mapa(x,y) == '.':
                # dostępne pole, sprawdzić czy już na liście
                for f in avfields:
                    if f.x==x and f.y==y: break
                else:
                    avfields.append(Field(x,y,self.dist+1,self.path+str(nei.index((x,y)))))
            elif mapa(x,y) == enemy:
                for u in aven:
                    if u.x==x and u.y==y: break
                else:
                    aven.append(FindUnit(x,y))
                    avenp.append(self.path)
        self.walked = True
    def __str__(self):
        return str(self.x) + "," + str(self.y) + " dist " + str(self.dist) + " path " + self.path
                    
#--------------------------------------------
class Unit:
    def __init__(self,x,y):
        self.x = x
        self.y = y
        self.hp = 200
        self.alive = True
        
    def __str__(self):
        return self.name+" at "+str(self.x)+","+str(self.y)+" HP:"+str(self.hp)

    def EnemyAdjacent(self):
        adj = False
        for (x,y) in AdjFields(self.x,self.y):
            adj = adj or (mapa(x,y) == self.enemy)
        return adj

    def TakeDamage(self,dam):
        self.hp = self.hp - dam
        if self.hp <= 0:
            self.alive = False
            board[self.y][self.x] = '.'
            units.remove(self)
            if self in unitsToMove:
                unitsToMove.remove(self)

    def TryAttack(self):
        attlist = list()
        if not self.alive: return
        for (x,y) in AdjFields(self.x,self.y):
            if mapa(x,y)==self.enemy:
                attlist.append(FindUnit(x,y))
        assert (len(attlist) > 0)
        attlist.sort(key=UnitHpKey)
        attlist[0].TakeDamage(self.ap)

    def TryMove(self):
        avfields = [Field(self.x,self.y,0,'')]
        aven = list()
        avenp = list()
        prevlen = 0
        while len(avfields) > prevlen:
            prevlen = len(avfields)
            for f in [a for a in avfields if not a.walked]:
                f.Walk(self.enemy,avfields,aven,avenp)
        if len(aven)>0:
            nei = AdjFields(self.x,self.y)
            board[self.y][self.x] = '.'
            self.x = nei[int(avenp[0][0])][0]
            self.y = nei[int(avenp[0][0])][1]
            assert(board[self.y][self.x] == '.')
            board[self.y][self.x] = self.type
                         
        
#---- End class Unit
            
class ElfUnit(Unit):
    def __init__(self,x,y):
        Unit.__init__(self,x,y)
        self.type = "E"
        self.enemy = "G"
        self.name = "Elf"
        self.ap = 3
    def EnemyCount(self):
        count = 0
        for u in units:
            if isinstance(u,GoblinUnit) and u.alive: count = count+1
        return count

class GoblinUnit(Unit):
    def __init__(self,x,y):
        Unit.__init__(self,x,y)
        self.type = "G"
        self.enemy = "E"
        self.name = "Goblin"
        self.ap = 3
    def EnemyCount(self):
        count = 0
        for u in units:
            if isinstance(u,ElfUnit) and u.alive: count = count+1
        return count


def BuildUnitList(board):
    units = list()
    for y in range(0,len(board)):
        for x in range(0,len(board[y])):
            if board[y][x] == 'E' : units.append(ElfUnit(x,y))
            elif board[y][x] == 'G' : units.append(GoblinUnit(x,y))
    return units

def FindUnit(x,y):
    for u in units:
        if u.x == x and u.y == y : return u

def UnitKey(u):
    return u.y*xsize + u.x

def UnitHpKey(u):
    return u.hp*barea + u.y*xsize + u.x

def mapa(x,y):
    return board[y][x]

board = readboard('input15.txt')
xsize = len(board[0])
ysize = len(board)
barea = xsize*ysize
units = BuildUnitList(board)
active = True
round = 0
while active:
    round = round+1
    print("round",round,"units left",len(units),"elves left",len([x for x in units if isinstance(x,ElfUnit)]))
    #printboard(board)
    #for u in units:
    #    print(u)
    #input()
    unitsToMove = units.copy()
    while len(unitsToMove) > 0:
        u = sorted(unitsToMove,key=UnitKey)[0]
        if u.EnemyCount() == 0:
            active = False
            round = round-1
            break
        if not u.EnemyAdjacent():
            u.TryMove()
        if u.EnemyAdjacent():
            u.TryAttack()
        unitsToMove.remove(u)

printboard(board)
sumhp = 0
for u in units:
    print(u)
    sumhp = sumhp+u.hp

print("round",round,"sumhp",sumhp,"answer",round*sumhp)
