def steps(rules):
  import string
  ndone = list(string.ascii_uppercase)
  done = ''
  while len(ndone)>0:
    for l in ndone:
      gotit = True
	  print('trying ',l)
      for (p,n) in rules:
        if n==l :
		  print('rule ',p,n)
          gotit = False
          break
      if gotit:
	    print('gotit ',l)
        ndone.remove(l)
        done = done+l
        for p,n in rules:
          if p==l :
            rules.replace((p,n))
        break
  return done