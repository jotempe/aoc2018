rec = [3,7]
c1 = 0
c2 = 1

def step(c):
    while c>= len(rec):
        c = c-len(rec)
    return c

target = 380621+10

while len(rec) < target:
    nr = rec[c1] + rec[c2]
    if nr > 9:
        rec.append(nr // 10)
        rec.append(nr % 10)
    else:
        rec.append(nr)
    c1 = step(c1+1+rec[c1])
    c2 = step(c2+1+rec[c2])

if len(rec) > target: rec.pop()

print(''.join(str(x) for x in rec[-10:]))
