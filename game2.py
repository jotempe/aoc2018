def game2(plcount,highmar):
    prev = [-1 for i in range(0,highmar+1)]
    next = [-1 for i in range(0,highmar+1)]
    score = {p:0 for p in range(1,plcount+1)}
    cplay = 0 
    next[0] = 0
    prev[0] = 0
    cpos = 0
    for m in range(1,highmar+1):
        cplay = cplay+1
        if cplay > plcount : cplay = 1
        if (m % 23) == 0 :
            score[cplay] = score[cplay]+m
            for i in range(0,7):
                cpos = prev[cpos]
            score[cplay] = score[cplay]+cpos
            next[prev[cpos]] = next[cpos]
            prev[next[cpos]] = prev[cpos]
            ppos = cpos
            cpos = next[cpos]
            next[ppos] = -1
            prev[ppos] = -1
        else:
            cpos = next[cpos]
            next[m] = next[cpos]
            prev[m] = cpos
            next[cpos] = m
            cpos = m
            prev[next[cpos]] = cpos
    maxscore = 0
    winner = 0
    for p in range(1,plcount+1):
        if score[p] > maxscore:
            maxscore = score[p]
            winner = p
    print("winner:",winner," score:",maxscore)
            
             
