class Room:
    def __init__(self,dist,fromroom,fromdir):
        self.dist = dist
        self.pdir = fromdir
        self.parent = fromroom
        self.doors = {fromdir:fromroom}
    def __repr__(self):
        return 'room at {0} doors {1}'.format(self.dist,{d:self.doors[d] for d in self.doors if d != self.pdir})

opposite = {'N':'S', 'S':'N', 'E':'W', 'W':'E'}

def buildtree(root):
    c = ''
    croom = root
    while c != '$':
        c = infile.read(1)
        if c in ('N','S','E','W'):
            if c in croom.doors:
                croom = croom.doors[c]
            else:
                croom.doors[c] = Room(croom.dist+1,croom,opposite[c])
                croom = croom.doors[c]
        elif c=='(':
            c = buildtree(croom)
            while c != ')':
                if c == '|':
                    c = buildtree(croom)
        else:
            return c
    return c

def visit(root,mindist):
    global maxdist
    sum = 0
    if root.dist>= mindist: sum = 1
    if root.dist>maxdist : maxdist = root.dist
    for d in root.doors:
        if d != root.pdir:
            sum = sum + visit(root.doors[d],mindist)
    return sum

import sys
sys.setrecursionlimit(5000)
infile = open("input20.txt")
c = infile.read(1)
print(c)
start = Room(0,None,"U")
c = buildtree(start)
print(c)
maxdist = 0
print(visit(start,1000))
print(maxdist)
