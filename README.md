# JMP's Advent of Code 2018 repo
Find here some code I used to solve the 2018 Advent of Code contest. The repository is in fact incomplete, sometimes the solution existed only as sequence of instruction typed in the console, or requires more than running the code (e.g. noticing some pattern in the results). 
And yes, I know it is a mess. Next year I'll start using repo from the very beginning, and not first when the problems become more difficult and it produces an obvious advantage.
I'll also try to make it more orderly, and with comments where useful.

Enjoy, JMP