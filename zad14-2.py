rec = [3,7]
c1 = 0
c2 = 1

def step(c):
    while c>= len(rec):
        c = c-len(rec)
    return c

target = '380621'
nchar = len(target)
done=False

while not done:
    nr = rec[c1] + rec[c2]
    if nr > 9:
        rec.append(nr // 10)
        pop = ''.join(str(x) for x in rec[-nchar:]) == target
        rec.append(nr % 10)
    else:
        rec.append(nr)
    c1 = step(c1+1+rec[c1])
    c2 = step(c2+1+rec[c2])
    if pop:
        rec.pop()
        done = True
    else:
        done = ''.join(str(x) for x in rec[-nchar:]) == target

print(len(rec)-nchar)
