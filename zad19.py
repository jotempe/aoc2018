class Processor:
    def __init__(self):
        self.reg = [0,0,0,0,0,0]
        self.instrs = [self.addr, self.addi, self.mulr, self.muli,
                       self.banr, self.bani, self.borr, self.bori,
                       self.setr, self.seti, self.gtir, self.gtri,
                       self.gtrr, self.eqir, self.eqri, self.eqrr]
        self.ip = 0

    def setreg(self,data):
        assert(len(data) == 6)
        for i in range(0,6):
            self.reg[i] = data[i]

    def getreg(self):
        return self.reg.copy()

    def addr(self,a,b,c):
        self.reg[c] = self.reg[a] + self.reg[b]

    def addi(self,a,b,c):
        self.reg[c] = self.reg[a] + b

    def mulr(self,a,b,c):
        self.reg[c] = self.reg[a] * self.reg[b]

    def muli(self,a,b,c):
        self.reg[c] = self.reg[a] * b

    def banr(self,a,b,c):
        self.reg[c] = self.reg[a] & self.reg[b]

    def bani(self,a,b,c):
        self.reg[c] = self.reg[a] & b

    def borr(self,a,b,c):
        self.reg[c] = self.reg[a] | self.reg[b]

    def bori(self,a,b,c):
        self.reg[c] = self.reg[a] | b

    def setr(self,a,b,c):
        self.reg[c] = self.reg[a]

    def seti(self,a,b,c):
        self.reg[c] = a

    def gtir(self,a,b,c):
        self.reg[c] = 1 if a > self.reg[b] else 0

    def gtri(self,a,b,c):
        self.reg[c] = 1 if self.reg[a] > b else 0

    def gtrr(self,a,b,c):
        self.reg[c] = 1 if self.reg[a] > self.reg[b] else 0

    def eqir(self,a,b,c):
        self.reg[c] = 1 if a == self.reg[b] else 0

    def eqri(self,a,b,c):
        self.reg[c] = 1 if self.reg[a] == b else 0

    def eqrr(self,a,b,c):
        self.reg[c] = 1 if self.reg[a] == self.reg[b] else 0

    def execute(self,program,ipr):
        # Program - lista instrukcji
        # ipr - numer rejestru zawierającego IP
        steps = 0
        pr0 = 0
        while self.ip<len(program):
            self.reg[ipr] = self.ip
            curr = program[self.ip]
            getattr(self, curr[0]) (curr[1],curr[2],curr[3])
            self.ip = self.reg[ipr]
            self.ip = self.ip+1
            steps = steps + 1
            #if steps%1000 == 0:
            #    print(steps, self.reg)
            if pr0 != self.reg[0] :
                print("r0 changed", steps ,self.reg)
                pr0 = self.reg[0]
        return steps
            

p = Processor()
filein=open("input19-1.txt")
prog = list()
for line in filein:
    if (line.startswith("#ip")):
        ipr = int(line.split(' ')[1])
    else:
        s = line.split(' ')
        instr = [s[0]] + [int(i) for i in s[1:]]
        prog.append(instr)

for l in prog:
    print(l)

p.setreg([1,0,0,0,0,0])
print(p.execute(prog,ipr))
print(p.getreg())

        
